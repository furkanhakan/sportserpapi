<?php

namespace App\Controller;

use App\Service\PlayerInjuriesService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class PlayerInjuriesController extends AbstractController
{
    /**
     * @param PlayerInjuriesService $playerInjuriesService
     * @param $playerId
     * @return JsonResponse
     * @Route("/api/PlayerInjuries/{playerId}",name="getPlayerInjuries")
     */
    public function index(PlayerInjuriesService $playerInjuriesService, $playerId)
    {
        $serviceResponse = $playerInjuriesService->getPlayerInjuries($playerId,$this->getParameter('transfermarktUrl'));
        return new JsonResponse($serviceResponse);
    }
}
