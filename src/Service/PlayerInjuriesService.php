<?php

namespace App\Service;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class PlayerInjuriesService
{

    /**
     * @var string
     */
    private $transfermarktUrl;


    /**
     * @param $playerId
     * @return array
     */
    public function getPlayerInjuries($playerId,$transfermarktUrl): ?array
    {
        $this->transfermarktUrl = $transfermarktUrl;
        $client = new Client([
            'base_uri' => $this->transfermarktUrl
        ]);
        $feedResponse = $client->get("{$playerId}/ajax/yw1/");
        $crawler = new Crawler($feedResponse->getBody()->getContents());
        $countPage = $crawler->filter('li.page')->count();
        if ($countPage == 0) {
            $countPage = 1;
        }
        try{
            $crawler->filter('#main > div:nth-child(17) > div.large-8.columns > div > div.table-header')->text();
        } catch (\Exception  $exception){
            return [
                "status" => [
                    "code" => 404,
                    "message" => "Player Not Found"
                ],
                "data" => []
            ];
        }
        for($i = 1; $i <= $countPage; $i++) {
            $feedResponse = $client->get("{$playerId}/ajax/yw1/page/{$i}");
            $crawler = new Crawler($feedResponse->getBody()->getContents());
            $crawler = $crawler->filter('#yw1 > table > tbody > tr ')->each(function (Crawler $node) {
                return $node;
            });
            $jsonData["status"] = [
                "code" => 200,
                "message" => "Success"
            ];
            foreach ($crawler as $item) {
                $jsonData["data"][] = [
                    "season" => $item->filter('td:nth-child(1)')->text(),
                    "injury" => $item->filter('td:nth-child(2)')->text(),
                    "dateFrom" => date('d/M/Y', strtotime($item->filter('td:nth-child(3)')->text())),
                    "dateUntil" => date('d/M/Y', strtotime($item->filter('td:nth-child(4)')->text())),
                    "days" => intval($item->filter('td:nth-child(5)')->text()),
                    "gameMissed" => intval($item->filter('td:nth-child(6)')->text())
                ];
            }
        }

        return $jsonData;
    }
}

